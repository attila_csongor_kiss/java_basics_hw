package com.epam.handson.part2.page35;

import java.util.Arrays;

public class Matrix {
	private double[][] matrix;
	
	public Matrix(final int size) {
		this.matrix = new double [size][size];	
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = 1;
			}
		}
	}
	
	public Matrix(double[][] matrix) {
		this.matrix = matrix;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(matrix);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matrix other = (Matrix) obj;
		if (!Arrays.deepEquals(matrix, other.matrix))
			return false;
		return true;
	}
	
	@Override
	public String toString() {		
		String ret = "";
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				ret += matrix[i][j] + " "; 
			}			
			ret += "\n";
		}
		
		return ret;
	}
	
	public boolean isSquareMatrix(double[][] matrix){				
		return matrix.length == matrix[0].length;
	}
	
	public double[][] transposeMatrix(double[][] matrix){		
		double[][] ret = new double [matrix[0].length][matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				ret[j][i] = matrix[i][j]; 
			}
		}
		
		return ret;
	}
	
	public double[][] evaluateMatrix(double[][] matrix){
		if(isSquareMatrix(matrix)){
			double[][] ret = new double [matrix.length][matrix[0].length];
			int sum = 0;
			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix[i].length; j++) {
					sum += matrix[i][j]; 
				}			
				matrix[i][i] = sum;
				sum = 0;
			}		
			return ret;
		} else {
			throw new IllegalArgumentException("The matrix has to be a square matrix to evaluate");
		}
	}
	
	public double[][] addMatrix(double[][] matrix_A, double[][] matrix_B){
		if(matrix_A.length == matrix_B.length && matrix_A[0].length == matrix_B[0].length){
			double[][] ret = new double [matrix_A.length][matrix_A[0].length];
			
			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix[0].length; j++) {
					ret[j][i] = matrix_A[i][j] + matrix_B[i][j]; 
				}
			}			
			return ret;
		} else {
			throw new IllegalArgumentException("The matrices have to be with the same size to add");
		}
	}
}
