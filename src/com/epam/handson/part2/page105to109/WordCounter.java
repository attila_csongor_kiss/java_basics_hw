package com.epam.handson.part2.page105to109;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WordCounter {

	public static void main(String[] args) {
		BufferedReader br = null;		
		String[] lineWords;
		Map<String, Integer> wordMap = new HashMap<>();
		int count;
		try {
			br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + args[0]));
			String line = null;
			while ((line = br.readLine()) != null) {
				lineWords = line.split(" ");
				for (int i = 0; i < lineWords.length; i++) {
					if(wordMap.containsKey(lineWords[i])) {
						count = wordMap.get(lineWords[i]);
						wordMap.put(lineWords[i], count + 1);
					} else {
						wordMap.put(lineWords[i], 1);
					}					
				}				 			
			 }
			
			String output = "{";
			for (Map.Entry<String, Integer> entry : wordMap.entrySet())
			{
			    output += entry.getKey() + "=" + entry.getValue() + ", ";
			}
			output = output.substring(0, output.length()-2);
			output += "}";
			System.out.println(output);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
