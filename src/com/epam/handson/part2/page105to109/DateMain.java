package com.epam.handson.part2.page105to109;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DateMain {

	public static void main(String[] args) {
		
		List<SimpleDate> dateList = new ArrayList<SimpleDate>();
		Random rn = new Random();
		
		for (int i = 0; i < 4; i++) {
			dateList.add(new SimpleDate(rn.nextInt(2100) + 1, rn.nextInt(11) + 1, rn.nextInt(30) + 1));
		}
		
		
		PrintWriter pw = null;
		
	    try {
	        pw = new PrintWriter(System.getProperty("user.dir") + "\\resources\\" + "input.txt");		
			
	        for(SimpleDate actDate : dateList) {
				pw.println(actDate);
			}
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } finally {
	        if (pw != null) pw.close();
	    }
		
				
		Collections.sort(dateList);
		
		try {
	        pw = new PrintWriter(System.getProperty("user.dir") + "\\resources\\" + "output.txt");		
			
	        for(SimpleDate actDate : dateList) {
				pw.println(actDate);
			}
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } finally {
	        if (pw != null) pw.close();
	    }
	}

}
