package com.epam.handson.part2.page105to109;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CharCounter {

	public static void main(String[] args) {		
		BufferedReader br = null;		
		String[] lineWords;
		int count;
		try {
			br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + args[0]));
			String line = null;
			 while ((line = br.readLine()) != null) {
				 lineWords = line.split(" ");
				 for (int i = 0; i < lineWords.length; i++) {
					count = 0;
					for (int j = 0; j < lineWords[i].length(); j++) {
						if((lineWords[i].charAt(j) > 'a' && lineWords[i].charAt(j) < 'z') 
								|| (lineWords[i].charAt(j) > 'A' && lineWords[i].charAt(j) > 'Z')) {
							count++;
						}
					}
					System.out.println(lineWords[i] + " -> " + count);
				}
				 
			 
			 }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	    
	}
}
