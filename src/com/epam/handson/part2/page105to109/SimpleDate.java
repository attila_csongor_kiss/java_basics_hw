package com.epam.handson.part2.page105to109;

public class SimpleDate implements Comparable<SimpleDate>{
	private int year;
	private int month;
	private int day;
	
	public SimpleDate(int year, int month, int day) {
		super();
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	
	public int getDay() {
		return day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}
	
	@Override
	public int compareTo(SimpleDate compareDate) {
		if(this.year < compareDate.getYear()) {
			return -1;
		} else if(this. year ==  compareDate.getYear()) {
			
			if(this.month < compareDate.getMonth()) {
				return -1;
			} else if(this.month == compareDate.getMonth()) {
				
				if(this.day < compareDate.getDay()){
					return -1;
				} else if(this.day == compareDate.getDay()) {
					return 0;
				} else if(this.day > compareDate.getDay()) {
					return 1;
				}
				
			} else if(this.month > compareDate.getMonth()) {
				return 1;
			}
			
		} else if(this.year > compareDate.getYear()){
			return 1;
		}		
		
		return 0;
	}

	@Override
	public String toString() {
		return "SimpleDate [year=" + year + ", month=" + month + ", day=" + day
				+ "]";
	}
}
