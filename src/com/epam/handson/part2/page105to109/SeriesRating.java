package com.epam.handson.part2.page105to109;

public class SeriesRating implements Comparable<SeriesRating>{
	private int season;
	private int episode;
	private String title;
	private double rating;
	
	public SeriesRating(int season, int episode, String title, double rating) {
		super();
		this.season = season;
		this.episode = episode;
		this.title = title;
		this.rating = rating;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getEpisode() {
		return episode;
	}

	public void setEpisode(int episode) {
		this.episode = episode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	@Override
	public int compareTo(SeriesRating compEpisode) {		
		if(this.rating < compEpisode.getRating()) {
			return -1;
		} else if(this.rating == compEpisode.getRating()) {
			return 0;
		} else {
			return 1;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + episode;
		long temp;
		temp = Double.doubleToLongBits(rating);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + season;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeriesRating other = (SeriesRating) obj;
		if (episode != other.episode)
			return false;
		if (Double.doubleToLongBits(rating) != Double
				.doubleToLongBits(other.rating))
			return false;
		if (season != other.season)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Episode [season=" + season + ", episode=" + episode
				+ ", title=" + title + ", rating=" + rating + "]";
	}
	
}
