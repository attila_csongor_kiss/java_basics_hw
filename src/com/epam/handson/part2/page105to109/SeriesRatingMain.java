package com.epam.handson.part2.page105to109;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class SeriesRatingMain {

	public static void main(String[] args) {
		PrintWriter pw = null;
		BufferedReader fbr;
		List<SeriesRating> episodesList = new ArrayList<SeriesRating>();
		List<SeriesRating> storedEpisodesList = new ArrayList<SeriesRating>();
		
	    try {	        
	    	fbr = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + "input.txt"));
	    	String line = null;
	    	String[] lineWords;
	    	SeriesRating maxRatedEpisode = null;
	    	
	        while ((line = fbr.readLine()) != null) {	            
	          if(!line.startsWith("#")){
	        	  lineWords = line.split(":");
	        	  episodesList.add(new SeriesRating(Integer.parseInt(lineWords[0]), Integer.parseInt(lineWords[1]), 
	        			  												lineWords[2], Double.parseDouble(lineWords[3])));	        	  
	          }
	        }	        
	        
	        pw = new PrintWriter(System.getProperty("user.dir") + "\\resources\\" + "input.txt");
	        
			InputStreamReader isr = new InputStreamReader(System.in);
		    BufferedReader br = new BufferedReader(isr);
		    String input = "";
		     
			do{
				System.out.println();
				System.out.println("[1] Store a new episode rating");
				System.out.println("[2] Save stored ratings");
				System.out.println("[3] Load ratings");
				System.out.println("[4] List ratings");
				System.out.println("[5] Find the best rated episode");
				System.out.println("[6] Details of an episode");
				System.out.println("[7] Modify details of an episode");
				System.out.println("[0] Exit");
				System.out.println("----------------------------------");
				System.out.println("Please choose:");
				
				input = br.readLine();
				
				if(input.equals("1")) {
					System.out.println("Please enter the episode details: ");
					input = br.readLine();
					lineWords = input.split(":");
					storedEpisodesList.add(new SeriesRating(Integer.parseInt(lineWords[0]), Integer.parseInt(lineWords[1]), 
								lineWords[2], Double.parseDouble(lineWords[3])));
				} else if(input.equals("2")) {
					for(SeriesRating actEpisode : storedEpisodesList){
			        	episodesList.add(actEpisode);
			        }
					storedEpisodesList.clear();
				} else if(input.equals("3")) {
					for(SeriesRating actEpisode : storedEpisodesList){
			        	System.out.println(actEpisode);
			        }
				} else if(input.equals("4")) {
			        for(SeriesRating actEpisode : episodesList){
			        	System.out.println(actEpisode);
			        }
				} else if(input.equals("5")) {
					for(SeriesRating actEpisode : episodesList){
						if(maxRatedEpisode == null) {
							maxRatedEpisode = actEpisode;
						}
						
			        	if(actEpisode.getRating() > maxRatedEpisode.getRating()) {
			        		maxRatedEpisode = actEpisode;
			        	}
			        }					
					System.out.println(maxRatedEpisode);
					
				} else if(input.equals("6")) {
					System.out.println("Please enter the name of the episode: ");
					input = br.readLine();
					for(SeriesRating actEpisode : episodesList){
			        	if(actEpisode.getTitle().equals(input)) {
			        		System.out.println(actEpisode);
			        	}
			        }
				} else if(input.equals("7")) {
					System.out.println("Please enter the name of the episode: ");
					input = br.readLine();
					for(SeriesRating actEpisode : episodesList){
			        	if(actEpisode.getTitle().equals(input)) {
			        		System.out.println("Please enter the new details: ");
			        		input = br.readLine();
			        		lineWords = input.split(":");
			        		actEpisode.setSeason(Integer.parseInt(lineWords[0]));
			        		actEpisode.setEpisode(Integer.parseInt(lineWords[1]));
			        		actEpisode.setTitle(lineWords[2]);
			        		actEpisode.setRating(Double.parseDouble(lineWords[3]));
			        	}
			        }
				}				
				
			}while(!(input.equals("0")));
	
			pw.println("# season : episode : title : rating");
			for(SeriesRating actEpisode : episodesList){
				pw.println(actEpisode.getSeason() + ":" + actEpisode.getEpisode() + ":" 
								+ actEpisode.getTitle() + ":" + actEpisode.getRating());
			}
			
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
			e.printStackTrace();
		} finally {
	        if (pw != null) pw.close();
	    }

	}

}
