package com.epam.handson.part2.page105to109;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringSorter {

	public static void main(String[] args) {
		BufferedReader br = null;		
		String[] lineWords;
		List<String> wordList = new ArrayList<String>();
	
		try {
			br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + args[0]));
			String line = null;
			String origWord;
			 while ((line = br.readLine()) != null) {
				 lineWords = line.split(" ");
				 for (int i = 0; i < lineWords.length; i++) {
					origWord = lineWords[i];
					lineWords[i] = "";
					for (int j = 0; j < origWord.length(); j++) {
						if((origWord.charAt(j) > 'a' && origWord.charAt(j) < 'z') 
								|| (origWord.charAt(j) > 'A' && origWord.charAt(j) > 'Z')) {
							lineWords[i] += origWord.substring(j, j+1);		
						}
					}
					
					if(!lineWords[i].equals("")) {
						wordList.add(lineWords[i]);
					}
					
				}				 				
			 }
			 
			 Collections.sort(wordList, ALPHABETICAL_ORDER);
			 
			 for(String word : wordList){
				 System.out.println(word);
			 }
			 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Comparator<String> ALPHABETICAL_ORDER = new Comparator<String>() {
	    public int compare(String str1, String str2) {
	        int res = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
	        if (res == 0) {
	            res = str1.compareTo(str2);
	        }
	        return res;
	    }
	};

}
