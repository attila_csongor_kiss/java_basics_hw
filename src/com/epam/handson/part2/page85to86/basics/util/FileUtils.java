package com.epam.handson.part2.page85to86.basics.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class FileUtils {

	public static void printStringLine(String path, String s){
		
		BufferedReader br = null; 
		
	    try {
	        br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path));
	        String line = null;
	        while ((line = br.readLine()) != null) {
	            if(line.contains(s)) System.out.println(line);
	        }
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
	
	public static void printStringCount(String path, String s){
	
		int count = 0;	
			
		BufferedReader br = null;
	
	    try {
	        br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path));
	        String line = null;
	        while ((line = br.readLine()) != null) {
	            if(line.contains(s)) count++;
	        }
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		
		System.out.println("Number of occurrences: " + count);
	}
	
	public static void printWriteFile(String path){
		PrintWriter pw = null;
	
	    try {
	        pw = new PrintWriter(System.getProperty("user.dir") + "\\resources\\" + path);		
			
			InputStreamReader isr = new InputStreamReader(System.in);
		    BufferedReader br = new BufferedReader(isr);
		    String input = "";
		     
			do{
				try {
					input = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				pw.println(input);
			}while(!(input.equals("")));
	
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } finally {
	        if (pw != null) pw.close();
	    }
	}
	
	public static void printNLine(String path, int N){
		
		BufferedReader br = null; 
		int count = 0;
		
	    try {
	        br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path));
	        String line = null;
	        while ((line = br.readLine()) != null && count < N) {
	            System.out.println(line);
	            count++;
	        }
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
	
	public static void printLastNLine(String path, int N){
		
		BufferedReader br = null; 
		int count = 0;
		int numberOfLines = 0;
		
	    try {
	        br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path));
	        String line = null;
	        while ((line = br.readLine()) != null) {	            
	        	numberOfLines++;
	        }
	        br.close();
	        br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path));
	        while ((line = br.readLine()) != null) {
	            if(count > numberOfLines - N - 1){
	            	System.out.println(line);
	            }
	            count++;
	        }
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
	
	public static void mergeTwoFilesIntoOne(String path_in1, String path_in2, String path_out){
		BufferedReader in1 = null;
		BufferedReader in2 = null;
		PrintWriter out = null;
		
		try {
			in1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path_in1));
			in2 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path_in2));
			out = new PrintWriter(System.getProperty("user.dir") + "\\resources\\" + path_out);
						 
		        try {
		        	String line = null;
					while ((line = in1.readLine()) != null) {	            
						out.println(line);
					}
					while ((line = in2.readLine()) != null) {	            
						out.println(line);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
		        
		   out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void l33t5p34kGenerator(String path){
		BufferedReader br = null; 
		
	    try {
	        br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\" + path));
	        String line = null;
	        String out = "";
	        while ((line = br.readLine()) != null) {
	            for (int i = 0; i < line.length(); i++) {
					if(line.substring(i, i+1).equals("s")){
						out += "z";
					} else if(line.substring(i, i+1).equals("k")){
						out += "x";
					} else if(line.substring(i, i+1).equals("s")){
						out += "z";
					} else if(line.substring(i, i+1).equals("a")){
						out += "@";
					} else if(line.substring(i, i+1).equals("e")){
						out += "3";
					} else if(line.substring(i, i+1).equals("i")){
						out += "1";
					} else if(line.substring(i, i+1).equals("o")){
						out += "0";
					} else if(line.substring(i, i+1).equals("u")){
						out += "v";
					} else if(line.substring(i, i+1).equals("f")){
						out += "ph";
					} else if(line.substring(i, i+1).equals("g")){
						out += "9";
					} else if(line.substring(i, i+1).equals("y")){
						out += "j";
					} else if(line.substring(i, i+1).equals("t")){
						out += "+";
					} else if(line.substring(i, i+1).equals("!")){
						out += "1";
					} else {
						out +=  line.substring(i, i+1);
					}
				}
	            line = "";
	            for (int i = 0; i < out.length(); i++) {
	            	if(i % 3 == 0){
	            		if(Character.isUpperCase(out.charAt(i))){
	            			line += out.substring(i, i+1).toLowerCase();
	            		} else {
	            			line += out.substring(i, i+1).toUpperCase();
	            		}
	            	} else {
	            		line += out.substring(i, i+1);
	            	}
	            }
	            
	            System.out.println(line);
	        }	       
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
}
