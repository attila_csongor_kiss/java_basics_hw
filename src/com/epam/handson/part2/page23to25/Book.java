package com.epam.handson.part2.page23to25;

import com.epam.handson.part2.page23to25.basics.util.genreEnum;
import com.epam.handson.part2.page23to25.basics.util.majorFormEnum;

public class Book {
	private String isbnNumber;
	private String title;
	private String author;
	private int numberOfPages;
	private String coverIllustrationURL;
	private majorFormEnum majorForm;
	private genreEnum genre;

	public Book(String isbnNumber) {
		super();
		this.isbnNumber = isbnNumber;
	}
	
	public String getIsbnNumber() {
		return isbnNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getCoverIllustrationURL() {
		return coverIllustrationURL;
	}

	public void setCoverIllustrationURL(String coverIllustrationURL) {
		this.coverIllustrationURL = coverIllustrationURL;
	}

	public majorFormEnum getMajorForm() {
		return majorForm;
	}

	public void setMajorForm(majorFormEnum majorForm) {
		this.majorForm = majorForm;
	}

	public genreEnum getGenre() {
		return genre;
	}

	public void setGenre(genreEnum genre) {
		this.genre = genre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((isbnNumber == null) ? 0 : isbnNumber.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (isbnNumber == null) {
			if (other.isbnNumber != null)
				return false;
		} else if (!isbnNumber.equals(other.isbnNumber))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Book [isbnNumber=" + isbnNumber + ", title=" + title
				+ ", author=" + author + ", numberOfPages=" + numberOfPages
				+ ", coverIllustrationURL=" + coverIllustrationURL
				+ ", majorForm=" + majorForm + ", genre=" + genre + "]";
	}
}
