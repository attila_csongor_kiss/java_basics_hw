package com.epam.handson.part2.page23to25.basics.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TrigonometricFunctions {
	public static void printTrigonometricValues() {
		 String input = "";
 		 
		 InputStreamReader isr = new InputStreamReader(System.in);
	     BufferedReader br = new BufferedReader(isr);
		 
	     
	     try {
			input = br.readLine();
		 } catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		 }
	    
	     while(!input.equals("END") && !input.equals("end")){				
			 System.out.println("Sin: " + Math.sin(Double.valueOf(input)));
			 System.out.println("Cos: " + Math.cos(Double.valueOf(input)));
			 System.out.println("Tg: " + Math.tan(Double.valueOf(input)));
			 try {
					input = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
		 }
		 
	}
}
