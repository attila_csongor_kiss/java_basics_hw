package com.epam.handson.part2.page23to25.basics.impl;

import java.io.File;

public class FileList {
	public static void listAllTheFiles(String suffix) {		
		File file = new File("./src/com/epam/handson/part_1/page_74_to_78");
		for(String actFile : file.list()) {
			if(actFile.endsWith(suffix.substring(1))) {
				System.out.println(actFile);
			}
		}				
	}
}
