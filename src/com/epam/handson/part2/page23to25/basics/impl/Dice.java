package com.epam.handson.part2.page23to25.basics.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Dice {
	public static void throwDice() {
		 String input = "";
		 		 
		 InputStreamReader isr = new InputStreamReader(System.in);
	     BufferedReader br = new BufferedReader(isr);
		 	     
	     Random rand = new Random();
	     
		 do{
			 System.out.println("What is your guess? (1-6)");
			 try {
				input = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}	
			if(!input.equals("")) System.out.println("You throw " + (rand.nextInt(6) + 1)); 
		 }while(!input.equals(""));
		 
	}
}
