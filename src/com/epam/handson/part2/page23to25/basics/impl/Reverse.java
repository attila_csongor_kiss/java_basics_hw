package com.epam.handson.part2.page23to25.basics.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Reverse {
	public static void readAndWriteReverseFomConsole() {
		 StringBuilder input = new StringBuilder("");
		 		 
		 InputStreamReader isr = new InputStreamReader(System.in);
	     BufferedReader br = new BufferedReader(isr);
		 
		 do{	
			 try {
				input = new StringBuilder(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
			 System.out.println(input.reverse());			
		 }while(!input.toString().equals(""));
		 
	}
}
