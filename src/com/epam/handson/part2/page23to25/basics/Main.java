package com.epam.handson.part2.page23to25.basics;

import com.epam.handson.part2.page23to25.Book;
import com.epam.handson.part2.page23to25.basics.impl.Dice;
import com.epam.handson.part2.page23to25.basics.impl.FileList;
import com.epam.handson.part2.page23to25.basics.impl.Reverse;
import com.epam.handson.part2.page23to25.basics.impl.TrigonometricFunctions;
import com.epam.handson.part2.page23to25.basics.util.genreEnum;
import com.epam.handson.part2.page23to25.basics.util.majorFormEnum;

public class Main {

	public static void main(String[] args){
		
		Reverse.readAndWriteReverseFomConsole();
		
		FileList.listAllTheFiles("*.java");
		
		TrigonometricFunctions.printTrigonometricValues();
		
		Dice.throwDice();
		
		Book book = new Book("0-943396-04-2");
		book.setAuthor("imre Mad�ch");
		book.setTitle("The Tragedy of Man");
		book.setCoverIllustrationURL(".../coverphoto.jpg");
		book.setMajorForm(majorFormEnum.DRAMA);
		book.setGenre(genreEnum.TRAGEDY);
		book.setNumberOfPages(273);
		System.out.println(book.toString());
	}

}
