package com.epam.handson.part2.page23to25.basics.util;

public enum genreEnum {
	COMEDY, DRAMA, EPIC, NONSENSE, LYRIC, MYTHOPOEIA, ROMANCE, SATIRE, TRAGEDY, TRAGICOMEDY
}
