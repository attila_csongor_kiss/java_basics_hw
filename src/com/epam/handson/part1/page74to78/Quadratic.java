package com.epam.handson.part1.page74to78;

public class Quadratic {

	public static void main(String[] args) {
		double[] result = Quadratic.sqroots(Double.valueOf(args[0]), Double.valueOf(args[1]), Double.valueOf(args[2])); 
		if(result.length == 0){
			System.out.println("There are no real roots of the quadratic formula");
		} else if(result.length == 1){
			System.out.println("The only root of the quadratic formula is: " + result[0]);
		} else {
			System.out.println("The roots of the quadratic formula are: " + result[0] + " and " + result[1]);
		}

	}
	
	private static double[] sqroots(final double a, final double b, final double c) {
		double[] ret = null;
		
		if(a == 0 || Quadratic.calculateDiscriminant(a, b, c) < 0) {
			ret = new double[0];
		} else if(Quadratic.calculateDiscriminant(a, b, c) == 0.0){
			ret = new double[1];
			ret[0] = (-b) / (2*a);
		} else if(Quadratic.calculateDiscriminant(a, b, c) > 0){
			ret = new double[2];
			ret[0] = ((-b) - Math.sqrt(Quadratic.calculateDiscriminant(a, b, c)))/ (2*a);
			ret[1] = ((-b) + Math.sqrt(Quadratic.calculateDiscriminant(a, b, c)))/ (2*a);
		}
		
		return ret;
	}
	
	private static double calculateDiscriminant(final double a, final double b, final double c) {
		return b*b - 4*a*c;
	}

}
