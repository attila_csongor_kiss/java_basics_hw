package com.epam.handson.part1.page74to78;

public class Euclid {

	public static void main(String args[]) {		
		if(args.length != 2 ) { 
			throw new IllegalArgumentException();
		}
		
		System.out.println("The greatest common divisor of numbert " + args[0] + " and " 
				+ args[1] + " is " + countEuclid(Integer.parseInt(args[0]),Integer.parseInt(args[1])));
		System.out.println("The greatest common divisor of numbert " + args[0] + " and " 
				+ args[1] + " is " + recursiveEuclid(Integer.parseInt(args[0]),Integer.parseInt(args[1])));
	}
	
	public static int countEuclid(int a, int b)
	{
		if( a == 0 )
			return b;
		
		while( b != a )
		{
			if( a > b ) 
				a -= b;
			else 
				b -= a;
		}
		
		return a;
	}
	
	public static int recursiveEuclid(int a, int b)
	{
		if( b == 0 )		
			return a;
		else 
			return recursiveEuclid(b, a % b);
	}

}
