package com.epam.handson.part1.page74to78;

public class Arrays {

	public static void main(String[] args) {
		 int[] inputArray = new int[args.length];
		 for(int i = 0; i < args.length; i++) {
			 inputArray[i] = Integer.parseInt(args[i]);
		 }
		 
		 System.out.println("The sum of the array is " + Arrays.arraySum(inputArray));
		 
		 System.out.println("The average of the array is " + Arrays.arrayAvg(inputArray));
		 
		 System.out.print("The norms of the elements of the array are ");
		 double[] normArray = Arrays.arrayNorm(inputArray);
		 for(int i = 0; i < normArray.length; i++){
			 System.out.print(normArray[i]);
			 if(i < normArray.length -1) {
				 System.out.print(", ");
			 }
		 }
		 System.out.println();
		 
		 System.out.println("The array is " + Arrays.join(inputArray));
		 
		 System.out.println("The array with delimiter is " + Arrays.join(inputArray, ";"));
			 
	}
	
	private static int arraySum(int[] array){
		int ret = 0;
		for(int i = 0; i < array.length; i++) {
			ret += array[i];
		}
		return ret;
	}
	
	private static int arrayAvg(int[] array){
		int ret = 0;
		for(int i = 0; i < array.length; i++) {
			ret += array[i];
		}
		return ret / array.length;
	}
	
	private static double[] arrayNorm(int[] array) {
		double[] ret = new double[array.length];
		
		int squareSum = 0;
		for(int i = 0; i < array.length; i++) {
			squareSum += array[i] * array[i];
		}
		
		for(int i = 0; i < ret.length; i++) {
			ret[i] = Double.valueOf(array[i]) / Double.valueOf(Math.sqrt(squareSum));
		}
		
		return ret;
	}
	
	private static String join(int[] array) {
		String ret = "";
		for(int i = 0; i < array.length; i++) {
			ret += array[i];
			if(i < array.length -1) {
				ret += ", ";
			}
		}
		return ret;
	}
	
	private static String join(int[] array, String delimiter) {
		String ret = "";
		for(int i = 0; i < array.length; i++) {
			ret += array[i];
			if(i < array.length -1) {
				ret += delimiter + " ";
			}
		}
		return ret;
	}
}
