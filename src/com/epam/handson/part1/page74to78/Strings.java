package com.epam.handson.part1.page74to78;

public class Strings {
	
	public static void main(String[] args) {
		
		System.out.println(capitalizeFirstChar(args[0]));
		
		System.out.println(changeHungarianCharacters(args[0]));
		
		System.out.println(encode(args[0]));
		System.out.println(decode(encode(args[0])));

	}

	private static String capitalizeFirstChar(String input) {
		return Character.toUpperCase(input.charAt(0)) + input.substring(1);
	}
	
	private static String changeHungarianCharacters(String input) {
		String ret = "";
		
		for (int i = 0; i < input.length(); i++) {
			if(input.charAt(i) == 'á') {
				ret += "a";
			} else if(input.charAt(i) == 'é') {
				ret += "e";
			} else if(input.charAt(i) == 'í') {
				ret+= "i";
			} else if(input.charAt(i) == 'ü' || input.charAt(i) == 'ű' || input.charAt(i) == 'ú') {
				ret+= "u";
			} else if(input.charAt(i) == 'ö' || input.charAt(i) == 'ő' || input.charAt(i) == 'ó') {
				ret+= "o";
			} else {
				ret += input.charAt(i);
			}
		}
		
		return ret;
	}
	
	private static String encode(String input){
		String ret = "";
		char code = 'a';
		
		for (int i = 0; i < input.length(); i++) {
			ret += (char) (input.charAt(i) ^ code);
		}
		
		return ret;
	}
	
	private static String decode(String input){
		String ret = "";
		char code = 'a';
		
		for (int i = 0; i < input.length(); i++) {
			ret += (char) (input.charAt(i) ^ code);
		}
		
		return ret;
	}
}
