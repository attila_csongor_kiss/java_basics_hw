package com.epam.handson.part1.page74to78;

public class MultiArrays {

	public static void main(String[] args) {
		
		double[][] arr = new double[3][3];
		arr[1][1] = 1;
		System.out.println( asMatrix( arr ) );
		
		int[][] arr2 = {{0, 1, 2} , {1, 2, 3}, {2, 3, 4}};
		System.out.println( sumMatrixPrint( arr2 ) );

	}
	
	private static String asMatrix(double[][] matrix) {
		String ret = "";
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				ret += matrix[i][j] + " "; 
			}			
			ret += "\n";
		}
		
		return ret;
	}
	
	private static String sumMatrixPrint(int[][] matrix) {
		String ret = "";
		int sum = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				ret += matrix[i][j] + " ";
				sum += matrix[i][j];
			}			
			ret += "| " + sum  + "\n";
			sum = 0;
		}
		
		for (int i = 0; i < matrix.length; i++) {
			ret += "__";
		}
		ret += "/\n";
		
		for (int j = 0; j < matrix.length; j++){
			for (int i = 0; i < matrix[j].length; i++) {
				sum += matrix[i][j];				
			}
			ret += sum + " ";
			sum = 0;
		}
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if(i == j) {
					sum += matrix[i][j];
				}
			}
		}
		
		ret += "	" + sum;
		return ret;
	}

}
