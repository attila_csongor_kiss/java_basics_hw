package com.epam.handson.part1.page53to55;

public class C2F {

	public static void main(String[] args) {
		if(args.length == 2){
			if(Integer.parseInt(args[0]) == 0){
				System.out.println(args[1] + " �C = " + ((Double.valueOf(args[1]) * 9.0/5.0) + 32.0) + " �F");				
			} else {
				System.out.println(args[1] + " �F = " + ((Double.valueOf(args[1]) - 32.0) * (5.0/9.0)) + " �C");
			}
		} else {
			throw new IllegalArgumentException("The program accepts two, and only two command line arguments");
		}

	}

}
