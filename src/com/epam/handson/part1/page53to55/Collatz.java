package com.epam.handson.part1.page53to55;

public class Collatz {
	
	public static void main(String[] args) {		
		int minusOneA = Integer.parseInt( args[0]);
		System.out.println(minusOneA);
		do{
			System.out.println(Collatz.sequenceNextElement(minusOneA));
		}while((minusOneA = Collatz.sequenceNextElement(minusOneA)) != 1);
		
	}
	
	public static int sequenceNextElement(int minusOneA) {			
		if(minusOneA % 2 == 0) {
			return minusOneA / 2;
		} else {
			return 3*minusOneA + 1;
		}	
	}

}
