package com.epam.handson.part1.page53to55;

public class PerfectNumber {

	public static void main(String[] args) {
		int count = 0;
		for(int i = 1; i < Integer.parseInt(args[0]); i++) {
			if(Integer.parseInt(args[0]) % i == 0) {
				count += i;
			}
		}
		if(count == Integer.parseInt(args[0])) {
			System.out.println(args[0] + " is a perfect number");
		} else {
			System.out.println(args[0] + " is not a perfect number");
		}		
	}
}
